#include <ll4ma_robot_interface/robot_interface.h>


#define POSE_3D_DOF 6

using std::string;

namespace robot_interface
{

  bool RobotInterface::init()
  {
    log("Initializing...");

    // read paramaters from ROS parameter server
    bool success = true;
    success &= nh_.getParam("tip_links", tip_links_);
    success &= nh_.getParam("root_links", root_links_);
    success &= nh_.getParam("torque_limits", torque_lims_);
    // only allegro supports q_max and q_min at the moment so this can't be required for successful initialization
    nh_.getParam("q_max", q_max_);
    nh_.getParam("q_min", q_min_);

    nh_.param<std::string>("jnt_cmd_topic", jnt_cmd_topic_, "joint_cmd");
    if (!success)
    {
      log("Could not load parameters from param server.", ERROR);
      return false;
    }

    std::string urdf_param;
    nh_.param<std::string>("urdf_param", urdf_param, "/robot_description");

    // Initialize KDL
    kdl_.reset(new manipulator_kdl::robotKDL(urdf_param, nh_, root_links_,
                                             tip_links_, gravity_));
    
    // Initialize Dartsim KD solver
    dart_kd_solver_.reset(new dart_kinematics_dynamics::DARTKinematicsDynamicsSolver(urdf_param));
    
    jnt_names_ = kdl_->getJointNames();
    num_jnts_ = jnt_names_.size();
    
    // Initialize Eigen
    tau_.setZero(num_jnts_);
    tau_g_.setZero(num_jnts_);

    // Initialize ROS
    initJointState(jnt_cmd_);
    jnt_cmd_pub_ = nh_.advertise<sensor_msgs::JointState>(jnt_cmd_topic_, 1);
    log("JOINT COMMAND TOPIC: " + jnt_cmd_topic_);

    log("Initialization complete.");
    return true;
  }


  void RobotInterface::saturateTorques(Eigen::VectorXd &torques)
  {
    for (int i = 0; i < num_jnts_; ++i)
      if (std::abs(torques[i]) > torque_lims_[i])
	torques[i] = copysign(torque_lims_[i], torques[i]);
  }


  void RobotInterface::publishTorqueCommand(Eigen::VectorXd &torques)
  {
    saturateTorques(torques);
    
    for (int i = 0; i < num_jnts_; ++i)
      jnt_cmd_.effort[i] = torques[i];
    jnt_cmd_pub_.publish(jnt_cmd_);
  }


  void RobotInterface::compensateGravity(Eigen::VectorXd &tau, Eigen::VectorXd &q)
  {
    kdl_->getGtau(0, q, tau_g_);
    tau += tau_g_;
  }


  void RobotInterface::clipJointsValues(Eigen::VectorXd &q)
  {
    for(int i = 0; i < q.size(); i++)
    {
        if(q[i] > q_max_[i])
        {
          q[i] = q_max_[i];
        } else if (q[i] < q_min_[i])
        {
          q[i] = q_min_[i];
        }
    } 
  }


  double RobotInterface::getNumJoints()
  {
    return num_jnts_;
  }

  
  std::vector<std::string> RobotInterface::getRootNames()
  {
    return root_links_;
  }

  
  std::vector<std::string> RobotInterface::getTipNames()
  {
    return tip_links_;
  }


  // void RobotInterface::getFK(Eigen::VectorXd &q, Eigen::Affine3d &x)
  // {
  //   getFK(0, q, x);
  // }


  void RobotInterface::getFK(Eigen::VectorXd &q, Eigen::Affine3d &x, const int &ch_idx)
  {
    kdl_->getFK(ch_idx, q, x);
  }


  // void RobotInterface::getJacobian(Eigen::VectorXd &q, Eigen::MatrixXd &J)
  // {
  //   kdl_->getJacobian(0, q, J);
  // }


  void RobotInterface::getJacobian(Eigen::VectorXd &q, Eigen::MatrixXd &J, const int &ch_idx)
  {
    kdl_->getJacobian(ch_idx, q, J);
  }


  void RobotInterface::getJointMass(Eigen::VectorXd &q, Eigen::MatrixXd &Mq)
  {
    kdl_->getM(0, q, Mq);
  }


  void RobotInterface::getTaskMass(Eigen::MatrixXd &Mq, Eigen::MatrixXd &J, Eigen::MatrixXd &Mx)
  {
    getPseudoInverse(J * Mq.inverse() * J.transpose(), Mx, 1.e-5);
  }


  void RobotInterface::getTaskError(Eigen::VectorXd &q, Eigen::VectorXd &q_dot,
                                    Eigen::Affine3d &x_des, Eigen::VectorXd &x_dot_des,
                                    Eigen::VectorXd &x_err, Eigen::VectorXd &x_dot_err)
  {
    kdl_->getTaskError(0, q, q_dot, x_des, x_dot_des, x_err, x_dot_err);
  }


  void RobotInterface::initJointState(sensor_msgs::JointState &jnt_state)
  {
    jnt_state = sensor_msgs::JointState();
    for (int i = 0; i < num_jnts_; ++i)
    {
      jnt_state.name.push_back(jnt_names_[i]);
      jnt_state.position.push_back(0.0);
      jnt_state.velocity.push_back(0.0);
      jnt_state.effort.push_back(0.0);
    }
  }


  void RobotInterface::log(std::string msg)
  {
    log(msg, INFO);
  }


  void RobotInterface::log(std::string msg, LogLevel level)
  {
    switch(level)
    {
      case WARN :
      {
        ROS_WARN_STREAM("[RobotInterface] " << msg);
        break;
      }
      case ERROR :
      {
        ROS_ERROR_STREAM("[RobotInterface] " << msg);
        break;    
      }
      default:
      {
        ROS_INFO_STREAM("[RobotInterface] " << msg);
        break;    
      }
    }
  }

  
  void RobotInterface::publishRealRobot(const Eigen::VectorXd &cmd, const Eigen::VectorXd &dot_cmd)
  {
    ROS_ERROR_STREAM("Function not defined");
  }

  
  void RobotInterface::publishRealRobot(const Eigen::VectorXd &cmd, ControlMode &cm)
  {
    ROS_ERROR_STREAM("Function not defined");
  }

  void RobotInterface::getDartGravityTorques(Eigen::VectorXd &gtau)
  {
    dart_kd_solver_->getGravityForces(jnt_names_, gtau);
  }

  void RobotInterface::getDartCoriolisTorques(Eigen::VectorXd &ctau)
  {
    dart_kd_solver_->getCoriolisForces(jnt_names_, ctau);
  }

  void RobotInterface::setDartJointState(const sensor_msgs::JointState &joint_state)
  {
    dart_kd_solver_->setJointState(joint_state.name, joint_state.position, joint_state.velocity);
  }
}
