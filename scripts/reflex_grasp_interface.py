#!/usr/bin/env python
import rospy
from std_srvs.srv import Empty, Trigger, TriggerResponse
from std_msgs.msg import Bool
from reflex_msgs.msg import PoseCommand, VelocityCommand, Hand
from ll4ma_robot_interface.srv import ReflexSetPreshape, ReflexSetPreshapeResponse


class ReflexGraspInterface:
    """
    This class exposes ROS services for performing higher-level actions 
    with the Reflex hand (e.g. grasp, release, open hand).
    """

    def __init__(self):
        """
        All initialization parameters are read from the ROS param server.
        """
        # Topics
        hand_state_topic = rospy.get_param("~hand_state_topic")
        pos_cmd_topic = rospy.get_param("~pos_cmd_topic")
        vel_cmd_topic = rospy.get_param("~vel_cmd_topic")
        grasp_status_topic = rospy.get_param("~grasp_status_topic")
        self.pos_cmd_pub = rospy.Publisher(pos_cmd_topic, PoseCommand, queue_size=1)
        self.vel_cmd_pub = rospy.Publisher(vel_cmd_topic, VelocityCommand, queue_size=1)
        self.status_pub  = rospy.Publisher(grasp_status_topic, Bool, queue_size=1)
        rospy.Subscriber(hand_state_topic, Hand, self._reflex_state_cb)

        # External services this class calls
        self.zero_tactile_srv = rospy.get_param("~zero_tactile_srv")
        self.calibrate_srv = rospy.get_param("~calibrate_srv")
        self.enable_tactile_stops_srv = rospy.get_param("~enable_tactile_stops_srv") 
        self.disable_tactile_stops_srv = rospy.get_param("~disable_tactile_stops_srv")

        # Internal services this class offers
        open_hand_srv = rospy.get_param("~open_hand_srv")
        grasp_srv = rospy.get_param("~grasp_srv")
        release_srv = rospy.get_param("~release_srv")
        set_preshape_srv = rospy.get_param("~set_preshape_srv")
        self.open_hand_srv = rospy.Service(open_hand_srv, Trigger, self.open_hand)
        self.grasp_srv = rospy.Service(grasp_srv, Trigger, self.grasp)
        self.release_srv = rospy.Service(release_srv, Trigger, self.release)
        self.set_preshape_srv = rospy.Service(set_preshape_srv, ReflexSetPreshape, self.set_preshape)
        self.rate = rospy.Rate(100)
        self.hand_state = None

        # TODO this is a huge hack just to monitor when Finger 2 goes bad. There is a firmware
        # issue such that at some point the tactile sensors just stop reading and the state
        # becomes fixed on some bogus values. Thus we keep a rolling queue of the sum of the
        # pressure values of that finger. There is always sensor noise, so if the finger is
        # functioning you'll get small fluctuations in the pressures and the sum of the
        # pressures therefore fluctuates. But, if it blows out, the values are fixed over the
        # full queue window and we know it needs to be powered off and on again until it's
        # resolved. Ideally the firmware issue can be fixed somehow.
        self.finger_2_is_blown = False
        self.finger_2_state_queue = range(100)

    def run(self):
        """
        Runs the node waiting for any external services that are used. 
        Also calibrates takktile sensors and fingers and enables tactile stops.
        """
        rospy.loginfo("Waiting for external services...")
        rospy.wait_for_service(self.zero_tactile_srv)
        rospy.wait_for_service(self.calibrate_srv)
        rospy.wait_for_service(self.enable_tactile_stops_srv)
        rospy.loginfo("Services are up!")

        self._zero_tactile()
        self._calibrate_fingers()
        self._enable_tactile_stops()
        
        rospy.loginfo("Waiting for hand state...")
        while not rospy.is_shutdown() and not self.hand_state:
            self.rate.sleep()
        rospy.loginfo("Hand state received!")

        rospy.loginfo("ReFlex hand interface services are available.")
        msg_shown = False
        while not rospy.is_shutdown():
            # Tell user when finger is messed up and needs to re-power
            if self.finger_2_is_blown and not msg_shown:
                rospy.logerr("ReFlex Finger is done. Need to power down and restart.")
                msg_shown = True
            self.rate.sleep()

    def stop(self):
        rospy.loginfo("Exiting.")

        
    # === BEGIN Service functions this class offers ===============================================
        
    def open_hand(self, req):
        """
        Service to open the hand all the way (i.e. move all three fingers to zero position).
        """
        pos_cmd = PoseCommand()
        pos_cmd.f1 = 0.0
        pos_cmd.f2 = 0.0
        pos_cmd.f3 = 0.0
        pos_cmd.preshape = 0.0
        for i in range(10):
            self.pos_cmd_pub.publish(pos_cmd)
        self.grasp_status = False
        return TriggerResponse(success=True)

    def grasp(self, req):
        """
        Service to grasp an object. Each finger closes until it makes contact and stops
        via the tactile stops, then all three fingers simultaneously tighten a small amount.
        """
        if self.finger_2_is_blown:
            rospy.logwarn("Cannot execute grasp: Finger2 is not working.")
            return TriggerResponse(success=False)
        
        self._close_hand()
        # Wait until all fingers are moving
        while self._all_fingers_stopped():
            self.rate.sleep()
        # Wait until they are all stopped
        while not self._all_fingers_stopped():
            self.rate.sleep()
        self._tighten_hand()

        return TriggerResponse(success=True)

    def release(self, req):
        """
        Service to release an object. All three fingers open until they are not in contact anymore.
        """
        if self.finger_2_is_blown:
            rospy.logwarn("Cannot execute grasp: Finger2 is not working.")
            return TriggerResponse(success=False)

        while not self._all_fingers_not_in_contact():
            self._loosen_hand()

        return TriggerResponse(success=True)

    def set_preshape(self, req):
        """
        Service to set preshape angle of the hand (three fingers in zero position).
        TODO: May eliminate the need for a separated srv type for this?
        """
        pos_cmd = PoseCommand()
        pos_cmd.f1 = 0.0
        pos_cmd.f2 = 0.0
        pos_cmd.f3 = 0.0
        pos_cmd.preshape = req.preshape
        for i in range(10):
            self.pos_cmd_pub.publish(pos_cmd)
        self.grasp_status = False
        return ReflexSetPreshapeResponse(success=True)
        
        
    # === END Service functions this class offers =================================================


    def _reflex_state_cb(self, hand_state):
        """
        Callback to save current hand state.
        """
        self.hand_state = hand_state
        self.finger_2_state_queue.append(sum(self.hand_state.finger[1].pressure))
        self.finger_2_state_queue.pop(0)
        if all(x == self.finger_2_state_queue[0] for x in self.finger_2_state_queue):
            self.finger_2_is_blown = True
    
    def _zero_tactile(self):
        """
        Makes service call to reflex driver to zero out takktile sensor readings.
        """
        try:
            zero_tactile = rospy.ServiceProxy(self.zero_tactile_srv, Empty)
            zero_tactile()
        except rospy.ServiceException as e:
            rospy.logwarn("Service request to zero ReFlex tactile failed: {}".format(e))
            return False       
        return True

    def _calibrate_fingers(self):
        """
        Makes service call to reflex driver to calibrate the finger encoders.
        """
        try:
            calibrate = rospy.ServiceProxy(self.calibrate_srv, Empty)
            calibrate()
        except rospy.ServiceException as e:
            rospy.logwarn("Service request to calibrate fingers failed: {}".format(e))
            return False
        return True

    def _enable_tactile_stops(self):
        """
        Makes service call to reflex driver to enable tactile stops on fingers.
        """
        try:
            enable_stops = rospy.ServiceProxy(self.enable_tactile_stops_srv, Empty)
            enable_stops()
        except rospy.ServiceException as e:
            rospy.logwarn("Service request to enable tactile stops failed: {}".format(e))
            return False
        return True

    def _disable_tactile_stops(self):
        """
        Makes service call to reflex driver to disable tactile stops on fingers.
        """
        try:
            disable_stops = rospy.ServiceProxy(self.disable_tactile_stops_srv, Empty)
            disable_stops()
        except rospy.ServiceException as e:
            rospy.logwarn("Service request to disable tactile stops failed: {}".format(e))
            return False
        return True

    def _all_fingers_in_contact(self):
        """
        Returns true if all fingers are in contact, false otherwise. 
        """
        f1_in_contact = sum(self.hand_state.finger[0].contact) > 0
        f2_in_contact = sum(self.hand_state.finger[1].contact) > 0
        f3_in_contact = sum(self.hand_state.finger[2].contact) > 0
        return f1_in_contact and f2_in_contact and f3_in_contact

    def _all_fingers_not_in_contact(self):
        """
        Returns true if all fingers are not in contact, false otherwise.
        """
        not_f1_in_contact = sum(self.hand_state.finger[0].contact) <= 0
        not_f2_in_contact = sum(self.hand_state.finger[1].contact) <= 0
        not_f3_in_contact = sum(self.hand_state.finger[2].contact) <= 0
        return not_f1_in_contact and not_f2_in_contact and not_f3_in_contact

    def _all_fingers_stopped(self, stop_threshold=10.0):
        """
        Returns true if all fingers have stopped moving, false otherwise.
        """
        f1_stopped = self.hand_state.motor[0].velocity < stop_threshold
        f2_stopped = self.hand_state.motor[1].velocity < stop_threshold
        f3_stopped = self.hand_state.motor[2].velocity < stop_threshold
        return f1_stopped and f2_stopped and f3_stopped

    def _close_hand(self, velocity=1.0):
        """
        Closes each finger until tactile contact is detected. Sends as velocity command.
        """
        self._zero_tactile()
        self._enable_tactile_stops()
        # Close the hand at constant velocity until it contacts object
        vel_cmd = VelocityCommand()
        vel_cmd.f1 = velocity
        vel_cmd.f2 = velocity  # bad finger
        vel_cmd.f3 = velocity
        vel_cmd.preshape = 0.0
        # Repeat command to ensure it's read by subscriber, sometimes fails if only done once
        for i in range(10):
            self.vel_cmd_pub.publish(vel_cmd)

    def _tighten_hand(self, pos_increment=0.8):
        """
        Closes each finger a small position increment to effectively tighten the grip.
        """
        self._disable_tactile_stops()
        pos_cmd = PoseCommand()
        pos_cmd.f1 = self.hand_state.finger[0].proximal + pos_increment
        pos_cmd.f2 = self.hand_state.finger[1].proximal + pos_increment
        pos_cmd.f3 = self.hand_state.finger[2].proximal + pos_increment
        pos_cmd.preshape = self.hand_state.motor[3].joint_angle
        for i in range(10):
            self.pos_cmd_pub.publish(pos_cmd)

    def _loosen_hand(self, pos_increment=-0.02):
        """
        Loosens each finger a small position increment.
        """
        self._disable_tactile_stops()
        pos_cmd = PoseCommand()
        pos_cmd.f1 = self.hand_state.finger[0].proximal + pos_increment \
                     if self.hand_state.finger[0].proximal > 0.1 else 0.0
        pos_cmd.f2 = self.hand_state.finger[1].proximal + pos_increment \
                     if self.hand_state.finger[1].proximal > 0.1 else 0.0
        pos_cmd.f3 = self.hand_state.finger[2].proximal + pos_increment \
                     if self.hand_state.finger[2].proximal > 0.1 else 0.0
        pos_cmd.preshape = self.hand_state.motor[3].joint_angle
        for i in range(10):
            self.pos_cmd_pub.publish(pos_cmd)
        
    
if __name__ == '__main__':
    rospy.init_node("reflex_grasp_interface")
    try:
        interface = ReflexGraspInterface()
        rospy.on_shutdown(interface.stop)
        interface.run()
    except rospy.ROSInterruptException:
        pass
        
       
